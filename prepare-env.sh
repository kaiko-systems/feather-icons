#!/bin/bash

WORKDIR=$PWD/.venv
NODE_VERSION=v16.16.0
NODEJS_URL="https://nodejs.org/dist/$NODE_VERSION/node-$NODE_VERSION-linux-x64.tar.xz"

function nodejs_check() {
    # download node to the local env if is not
    if ! [ -x $WORKDIR/bin/node ]; then
        echo "Downloading Nodejs..."
        curl -SL --progress-bar $NODEJS_URL | tar xJ --strip-components 1 -C $WORKDIR/
        echo "DONE!"
    fi
}

if [ ! -d "$WORKDIR" ]; then
    mkdir $WORKDIR
fi
nodejs_check

export PATH=$WORKDIR/bin:$PATH
